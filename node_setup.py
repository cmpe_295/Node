# For executing external programs and files.
import subprocess

# Import JSON.
import json

# For exiting the program.
import sys

# Used for log messages.
import time

# Used to download Docker CE 17.03.0.
import urllib2

# Setting up Docker CE.
def install_Docker():

    # Required package.
    subprocess.call("apt-get -y install libltdl7", shell=True)

    # Download Docker CE 17.03.0.
    url = "https://download.docker.com/linux/ubuntu/dists/xenial/pool/stable/amd64/docker-ce_17.03.0~ce-0~ubuntu-xenial_amd64.deb"
    filedata = urllib2.urlopen(url)
    datatowrite = filedata.read()

    # Save Docker CE.
    with open("./docker-ce_17.03.0_ce-0_ubuntu-xenial_amd64.deb", 'wb') as f:
        f.write(datatowrite)

    # Change the permissions for the file.
    subprocess.call("sudo chmod go+rwx ./docker-ce_17.03.0_ce-0_ubuntu-xenial_amd64.deb", shell=True)

    # Install Docker CE 17.03.0.
    subprocess.call("sudo dpkg -i ./docker-ce_17.03.0_ce-0_ubuntu-xenial_amd64.deb", shell=True)

    # Create the docker group.
    subprocess.call("sudo groupadd docker", shell=True)

    # Add the user to the docker group.
    subprocess.call("sudo usermod -aG docker $USER", shell=True)

    # Automatically start the Docker engine on boot.
    subprocess.call("sudo systemctl enable docker", shell=True)

    # Install the Linux volume manager and start the service.
    subprocess.call("sudo apt-get -y install lvm2", shell=True)
    subprocess.call("sudo systemctl start lvm2-lvmetad.service", shell=True)

    # Create the Docker daemon file.
    docker_daemon = {
      "storage-driver": "devicemapper"
    }

    # Try to generate the daemon file for Docker (daemon.json).
    try:
        with open('./daemon.json', 'w') as outfile:
            json.dump(docker_daemon, outfile, indent=4)

    except:
        f.write("\n%s Could not create the daemon file for Docker.\n" % time.asctime(time.localtime(time.time())))
        sys.exit()

    else:

        # Change the permissions for the file.
        subprocess.call("sudo chmod go+rwx ./daemon.json", shell=True)

        # Copy the configuration file for the Docker daemon to /etc/docker.
        subprocess.call("sudo cp ./daemon.json /etc/docker", shell=True)

        # Remove the local configuration file.
        subprocess.call("sudo rm -R ./daemon.json", shell=True)

# Check for pip.
def check_for_pip():

    try:
        import pip

    except ImportError:
        subprocess.call("sudo apt-get install -y python-pip", shell=True)

# Check for pip3.
def check_for_pip3():

    try:
        import pip3

    except ImportError:
        subprocess.call("sudo apt-get install -y python3-pip", shell=True)

# Check for the required Python modules.
def install_modules():

    # Install boto3 for python3 and python2.7.
    try:
        subprocess.call("sudo pip3 install boto3", shell=True)
    except:
        print("boto3 for pip3 is already installed")

    try:
        subprocess.call("sudo pip install boto3", shell=True)
    except:
        print("boto3 for pip is already installed")

    # Install PyYAML (yaml is imported) for python3 and python2.7.
    try:
        subprocess.call("sudo pip3 install PyYAML", shell=True)
    except:
        print("PyYAML for pip3 is already installed")
    try:
        subprocess.call("sudo pip install PyYAML", shell=True)
    except:
        print("PyYAML for pip is already installed")

# Try to open the log file.
try:
    f = open('node_setup_log.txt', 'w')
    subprocess.call("sudo chmod go+rwx ./node_setup_log.txt", shell=True)
    print( "\n%s The log file node_setup_log.txt was successfully created.\n" % time.asctime(time.localtime(time.time())) )

except:
    print( "\n%s Could not create the log file node_setup_log.txt.\n" % time.asctime(time.localtime(time.time())) )
    sys.exit()

def main():

    print("\n%s Started the setup for the node.\n" % time.asctime(time.localtime(time.time())))
    f.write("\n%s Started the setup for the node.\n" % time.asctime(time.localtime(time.time())))
    subprocess.call("apt-get update", shell=True)
    install_Docker()
    check_for_pip()
    check_for_pip3()
    install_modules()
    print( "\n%s Finished installing the dependencies for the node.\n" % time.asctime(time.localtime(time.time())) )
    f.write("\n%s Finished installing the dependencies for the node.\n" % time.asctime(time.localtime(time.time())))
    f.close()

if __name__ == '__main__':
    main()
