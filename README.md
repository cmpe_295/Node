CMPE 295 Node

Getting Started

This program installs the shared software between the Greengrass core and Greengrass devices on the fog node/client.

Prerequisites

The client must have Git installed to clone the repo.

Installing

This repo is cloned when the install dependencies script is executed
(ecep_client_cpu/ecep_client/install_dependencies.sh).

The Python file node_setup.py is executed by
ecep_client_cpu/ecep_client/install_dependencies.sh.

Versioning

v1.0

Authors

Sona Bhasin
Kanti Bhat
Johnny Nigh
Monica Parmanand
